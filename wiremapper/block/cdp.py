from pockethernet import CdpResult
from wiremapper.block.block import Block
import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, GObject, Gio, GdkPixbuf


class CdpBlock(Block):
    def make(self, result):
        if not isinstance(result, CdpResult):
            return

        listbox = Gtk.ListBox()
        listbox.get_style_context().add_class('content')

        grid = Gtk.Grid()
        grid.set_row_spacing(12)
        grid.set_column_spacing(12)

        i = 0
        for type, name, data in result.field_list:
            grid.attach(self._dim_label(str(type), xalign=1), 0, i, 1, 1)
            grid.attach(self._dim_label(str(name), xalign=1), 1, i, 1, 1)

            if isinstance(data, list):
                temp = []
                for item in data:
                    temp.append(str(item))
                data = ', '.join(temp)

            grid.attach(Gtk.Label(str(data), xalign=1), 2, i, 1, 1)
            i += 1

        self._add_simple_row(listbox, grid)

        listbox.set_selection_mode(Gtk.SelectionMode.NONE)

        return self._make_result("CDP", listbox)
